FROM python:3.7

COPY ./common /common
COPY ./reddit /reddit
COPY ./requirements.txt /reddit
WORKDIR /reddit
RUN pip install -r requirements.txt

ENV DBHOST placeholder
ENV DBDATABASE placeholder
ENV DBUSER placeholder
ENV DBPASS placeholder
ENV DBPORT 3306

EXPOSE 5000
ENTRYPOINT ["python"]

CMD ["main.py"]