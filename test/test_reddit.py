
from reddit.reddit_db import SubredditPosts
from reddit.push_shift import PshiftApi


def test_PshiftApi():
    p = PshiftApi("unikoingold")
    posts = SubredditPosts("unikoingold")
    submissions = p.get_submissions()
    appended = True
    while submissions is not None and appended:
        appended = posts.append_posts(submissions)
        before = posts.earliest_post_timestamp()
        submissions = p.get_submissions(before)
    print(submissions)
