from flask import Flask, Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
from reddit.data_collection import update_reddit_posts

scheduler = BackgroundScheduler()
app = Flask(__name__)


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running:
        return Response("{}", status=500, mimetype='application/json')
    return Response("{}", status=200, mimetype='application/json')

now = datetime.datetime.now()

if __name__ == '__main__':
    scheduler.add_job(update_reddit_posts, start_date=now + datetime.timedelta(minutes=1), trigger='interval', minutes=120, max_instances=1)
    scheduler.start()
    app.run(host='0.0.0.0', debug=True, port=5000)
