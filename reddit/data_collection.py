import datetime
from typing import List, Union

from reddit.push_shift import PshiftApi
from reddit.reddit_db import get_master_spreadsheet, get_coin_id_for_reddit, get_reddit_entry_for_coin_id, \
    save_reddit_information, SubredditPosts, RedditPost
from common.config import format_logger
import logging

logger = logging.getLogger('data_collection')
format_logger(logger)


def validate_coin(coin: dict) -> Union[None,int]:
    # Not interested in coins that don't have symbols
    
    logger.info('validate_coin: {}'.format(coin["reddit"]))

    if coin["reddit"] is None or coin["reddit"] == "":
        logger.info("Coin: {} did not have a reddit listed".format(coin["symbol"]))
        return None
    # check for coin_id in database
    coin_id = get_coin_id_for_reddit(coin["reddit"], coin["symbol"])

    return coin_id


def fill_before(coin, reddit_posts):
    logger.info('Filling subreddit info before earliest')
    pshift = PshiftApi(coin["reddit"])
    if reddit_posts is None:
        logger.info(
            "Found no information for subreddit {}, beginning collecting it all".format(coin["reddit"]))
        reddit_posts = SubredditPosts(coin["reddit"])
        before = datetime.datetime.now().timestamp()
    else:
        before = reddit_posts.earliest_post_timestamp

    submissions: List[RedditPost] = pshift.get_submissions(before=before)
    appended = True
    while submissions is not None and appended:
        logger.info('Found {} submissions'.format(len(submissions)))
        appended = reddit_posts.append_posts(submissions)

        if len(submissions) > 0:
            before = min([p.timestamp for p in submissions])
        else:
            datetime.datetime.now().timestamp()
        submissions = pshift.get_submissions(before=before)

    reddit_posts.sort_posts()
    return reddit_posts


def fill_after(coin, reddit_posts):
    logger.info('Filling subreddit info after latest')
    pshift = PshiftApi(coin["reddit"])
    if reddit_posts is None:
        return None
    else:
        # if exists pull out the last 3 week's worth of posts and check them for updates
        logger.info("Collecting past 3 weeks of information for {}".format(coin["reddit"]))
        latest_ts = reddit_posts.latest_post_timestamp or datetime.datetime.utcnow().timestamp()
        latest_dt = datetime.datetime.utcfromtimestamp(latest_ts)
        after = int((latest_dt - datetime.timedelta(days=21)).timestamp())

    submissions: List[RedditPost] = pshift.get_submissions(after=after)
    appended = True
    while submissions is not None and appended:
        logger.info('Found {} submissions'.format(len(submissions)))
        appended = reddit_posts.append_posts(submissions)

        if len(submissions) > 0:
            after = max([p.timestamp for p in submissions])
        else:
            datetime.datetime.now().timestamp()
        submissions = pshift.get_submissions(after=after)

    reddit_posts.sort_posts()
    return reddit_posts


def update_reddit_posts():

    coin_sheet = get_master_spreadsheet()

    print('coin_sheet len: {}'.format(len(coin_sheet)))

    for coin in coin_sheet:

        try: 
            # if coin['reddit'] != 'mobilegoofficial': continue
            
            logger.info("Getting reddit info for {}".format(coin["reddit"]))
            
            coin_id = validate_coin(coin)
            
            if coin_id is None:

                logger.error("Coin id for {} is non-existent, skipping".format(coin["reddit"]))
                continue

            reddit_posts = get_reddit_entry_for_coin_id(coin_id)

            reddit_posts = fill_before(coin, reddit_posts)
            reddit_posts = fill_after(coin, reddit_posts)

            # put entry into database
            save_reddit_information(coin_id, reddit_posts)

        except Exception as error:

            print(error)