import os
import logging

DBHOST = os.environ.get('DBHOST', 'rootmont-dev2.c92fgo0uigpd.us-west-1.rds.amazonaws.com')
DBDATABASE = os.environ.get('DBDATABASE', 'api')
DBUSER = os.environ.get('DBUSER', 'rootmont')
DBPASS = os.environ.get('DBPASS', '')
if DBPASS == '':
    print("DATABASE PASSWORD WAS NOT SET. Please set this in the environment.")
DBPORT = os.environ.get('DBPORT', 3306)

gsheet_url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTGh4W0Nfq3v-AxXVXDoCNjrIx9Y8B5DO_nCjJaLlSTYhdV97fZYHfNu2JAPfl0-R_yMH5A3DQk0fn0/pub?gid=0&single=true&output=tsv'
