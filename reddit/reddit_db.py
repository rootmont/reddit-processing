import logging
import datetime
import requests
import json
from typing import Union, List, Set

from reddit.env import gsheet_url
from common.config import format_logger
from common.db.connect import connection_wrap, timing

logger = logging.getLogger('reddit_db')
format_logger(logger)


class RedditComment(object):
    def __init__(self, identifier: str, author: str, time: datetime.datetime):
        self._identifier: str = identifier
        self._author: str = author
        self._time: datetime.datetime = time

    def __eq__(self, other):
        if type(other) != RedditComment:
            return False
        if self._author == other.author and self._time.timestamp() == other.time.timestamp() and \
                self._identifier == other.identifier:
            return True
        return False

    def hash(self):
        return hash(self._author.__hash__() + self._time.__hash__())

    @property
    def author(self):
        return self._author

    @property
    def time(self):
        return self._time

    @property
    def identifier(self):
        return self._identifier

    def before(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() < day

    @classmethod
    def from_json(cls, j: dict):
        return cls(j["id"], j["author"], datetime.datetime.utcfromtimestamp(j["time"]))

    def to_json(self):
        return {
            'id': self._identifier,
            'author': self._author,
            'time': int(self._time.timestamp())
        }


class RedditPost(object):
    def __init__(self, identifier: str, author: str, time: Union[datetime.datetime, str], comments: int):
        self._identifier = identifier
        self._author = author
        if type(time) is not datetime.datetime:
            self._time = datetime.datetime.utcfromtimestamp(int(time))
        else:
            self._time: datetime.datetime = time
        self._comments: int = comments

    def __eq__(self, other):
        if type(other) != RedditPost:
            return False
        if self._identifier == other.identifier:
            return True
        return False

    def __hash__(self):
        return hash(self._identifier.__hash__() + self._author.__hash__() + self.timestamp.__hash__())

    def __str__(self):
        return "<RedditPost, id: {} author: {} timestamp: {} comments: {}>"\
            .format(self._identifier, self._author, self.timestamp, self._comments)

    @property
    def comments(self):
        return self._comments

    @property
    def author(self):
        return self._author

    @property
    def time(self):
        return self._time

    @property
    def timestamp(self):
        return int(self._time.timestamp())

    @property
    def identifier(self):
        return self._identifier

    """
    def append_comments(self, comments) -> None:
        if type(comments) == List:
            self._comments += comments
        else:
            self._comments.append(comments)
    """

    def before(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() < day

    def after(self, day: Union[datetime.datetime, datetime.date]) -> bool:
        if type(day) == datetime.datetime:
            day = day.date()
        return self._time.date() >= day

    """
    def comments_before(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditComment]:
        return list(filter(lambda x: x.before(day), self._comments))
    """

    @classmethod
    def from_json(cls, j: dict):
        return cls(
            j["id"],
            j["author"],
            datetime.datetime.utcfromtimestamp(j["time"]),
            j["comments"]
        )

    def to_json(self):
        return {
            'id': self._identifier,
            'author': self._author,
            'time': int(self._time.timestamp()),
            'comments': self._comments,
        }


class SubredditPosts(object):
    def __init__(self, subreddit: str, posts: List[RedditPost] = None):
        self._subreddit: str = subreddit
        if posts is None or len(posts) == 0:
            self._reddit_posts: List[RedditPost] = []
            self._reddit_post_ids: List[str] = []
            self._earliest_post_timestamp = None
            self._latest_post_timestamp = None
        else:
            self._reddit_posts: List[RedditPost] = posts
            self._reddit_post_ids: Set[str] = {x.identifier for x in posts}
            self._earliest_post_timestamp = min([x.timestamp for x in posts])
            self._latest_post_timestamp = max([x.timestamp for x in posts])

    @timing
    def append_posts(self, posts: List[RedditPost]) -> bool:
        """
        Appends new posts to
        :param posts: RedditPosts to add to the reddit_posts variable
        :return: bool - whether or not new posts were added
        """
        appended = False
        for p in posts:
            if p.identifier in self._reddit_post_ids:
                appended = appended or self.__update_post(p)
            else:
                appended = True
                self._reddit_posts.append(p)
                self._reddit_post_ids.add(p.identifier)
                if self.earliest_post_timestamp is None or p.timestamp < self.earliest_post_timestamp:
                    self._earliest_post_timestamp = p.timestamp
                if self.latest_post_timestamp is None or p.timestamp > self.latest_post_timestamp:
                    self._latest_post_timestamp = p.timestamp
        return appended

    @timing
    def sort_posts(self):
        self._reddit_posts.sort(key=lambda x: x.timestamp, reverse=True)

    def posts_before(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditPost]:
        if type(day) == datetime.datetime:
            day = day.date()
        return list(filter(lambda x: x.before(day), self._reddit_posts))

    def posts_after(self, day: Union[datetime.datetime, datetime.date]) -> List[RedditPost]:
        if type(day) == datetime.datetime:
            day = day.date()
        return list(filter(lambda x: x.after(day), self._reddit_posts))

    def __update_post(self, post: RedditPost) -> bool:
        updated = False
        for index in range(len(self._reddit_posts)):
            if self._reddit_posts[index] == post:
                # Interested if there were any comments updated
                if self._reddit_posts[index].comments != post.comments:
                    self._reddit_posts[index] = post
                    updated = True
                    logger.debug("Updating post: {}".format(post))
                    break
        return updated

    @property
    def posts(self):
        return self._reddit_posts

    @property
    def earliest_post_timestamp(self):
        return self._earliest_post_timestamp

    @property
    def latest_post_timestamp(self):
        return self._latest_post_timestamp

    @property
    def subreddit(self):
        return self._subreddit

    @classmethod
    def from_json(cls, j: dict):
        return cls(j["subreddit"], [RedditPost.from_json(p) for p in j["posts"]])

    def to_json(self):
        return {
            'subreddit': self._subreddit,
            'posts': [p.to_json() for p in self._reddit_posts]
        }


def get_master_spreadsheet():
    logger.info('Acquiring spreadsheet')
    ret = []
    response = requests.get(gsheet_url)
    text = response.text
    for line in text.split('\n')[1:]:
        cols = line.split('\t')
        symbol, name, industry, ico, mineable = cols[:5]
        open_source, ico_raise, max_supply, hash_algorithm, blockchain_type = cols[5:10]
        disclosure, execution, potential, github_url, description = cols[10:15]
        website, whitepaper, contract_addr, twitter, reddit, telegram, _, _, _ = cols[15:]
        website = website.replace('\n','').replace('\r','')
        whitepaper = whitepaper.replace('\n','').replace('\r','')
        twitter = twitter.replace('\n','').replace('\r','')
        reddit = reddit.replace('\n','').replace('\r','')
        telegram = telegram.replace('\n','').replace('\r','')
        symbol = symbol.strip()
        name = name.strip()
        industry = industry.strip()
        hash_algorithm = hash_algorithm.strip()
        blockchain_type = blockchain_type.strip()
        contract_addr = contract_addr.strip()
        twitter = twitter.strip()
        reddit = reddit.strip()
        telegram = telegram.strip()
        if not ico.isdigit(): ico = 0
        if not mineable.isdigit(): mineable = 0
        if not open_source.isdigit(): open_source = 0
        if not ico_raise.isdigit(): ico_raise = 0
        if not max_supply.isdigit(): max_supply = 0
        if not disclosure.isdigit(): disclosure = 0
        if not execution.isdigit(): execution = 0
        if not potential.isdigit(): potential = 0
        ret.append({
            'symbol': symbol,
            'name': name,
            'ico': int(ico),
            'mineable': int(mineable),
            'industry': industry,
            'open_source': int(open_source),
            'ico_raise': int(ico_raise),
            'max_supply': int(max_supply),
            'hash_algorithm': hash_algorithm,
            'blockchain_type': blockchain_type,
            'disclosure': int(disclosure),
            'execution': int(execution),
            'potential': int(potential),
            'github_url': github_url,
            'description': description,
            'website_url': website,
            'whitepaper_url': whitepaper,
            'contract_address': contract_addr,
            'twitter': twitter,
            'reddit': reddit,
            'telegram': telegram
        })
    return ret


@connection_wrap
def get_coin_id_for_reddit(reddit, ticker_symbol=None, cursor=None):

    if ticker_symbol is None:
        
        sql = "SELECT coin_id from coin where reddit = '{}'".format(reddit)

    else:
        
        sql = "SELECT coin_id from coin where reddit = '{}' and ticker_symbol = '{}'".format(reddit,ticker_symbol)
    
    cursor.execute(sql)

    coin_id = cursor.fetchone()
        
    logger.info('coin_id: {} sql: {} reddit: {}'.format(coin_id, sql, reddit))

    return coin_id if coin_id is None else coin_id['coin_id']


@connection_wrap
def get_reddit_entry_for_coin_id(coin_id, cursor=None) -> Union[None, SubredditPosts]:
    try:
        sql = "SELECT posts from reddit where coin_id = %s"
        cursor.execute(sql, [coin_id])
        reddit_raw = cursor.fetchone()

        print('reddit_raw: {} sql: {} coin_id: {}'.format(reddit_raw, sql, coin_id))

        if reddit_raw is None:

            return None

        return SubredditPosts.from_json(json.loads(reddit_raw["posts"]))

    except Exception as error:

        print(error)


@connection_wrap
def save_reddit_information(coin_id: int, reddit_info: SubredditPosts, cursor = None) -> None:
    sql = "INSERT INTO reddit (coin_id, subreddit, posts) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE posts = %s"
    j = json.dumps(reddit_info.to_json())
    cursor.execute(sql, [coin_id, reddit_info.subreddit, j, j])
    logger.info("Saved reddit info for {}".format(reddit_info.subreddit))
