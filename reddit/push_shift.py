import requests
import logging
import json
import datetime
from typing import List, Union

from ratelimit import limits, sleep_and_retry, RateLimitException
from reddit.reddit_db import RedditComment, RedditPost
from common.config import format_logger

logger = logging.getLogger('reddit')
format_logger(logger)

class PshiftApi(object):
    def __init__(self, subreddit: str):
        self._subreddit: str = subreddit
        self._session = requests.Session()
        self._reddit_session = requests.Session()
        self._base_url: str = "https://api.pushshift.io/reddit"

    @sleep_and_retry
    @limits(calls=150, period=60)
    def _api_call(self, endpoint: str, parameters: dict = None):
        """
        Allows for an api call once per second as recommended by the pushshift host
        :return: the response from the call
        """
        # logger.info("Parameters: {}".format(parameters))
        resp = self._session.get(self._base_url + endpoint + self._params_to_query(parameters))
        if resp.status_code == 429:
            raise RateLimitException
        if resp.status_code != 200:
            logger.error("Could not access the PshiftAPI, got {}".format(resp.status_code))
            return None
        return json.loads(resp.text)

    def _params_to_query(self, params: dict = None):
        if params is None:
            return ""
        if len(params) == 0:
            return ""
        params_list = []
        for k, v in params.items():
            params_list.append(str(k) + "=" + str(v))
        return "?" + "&".join(params_list)

    def get_all_comment_ids(self, submission_id: str):
        resp = self._api_call("/submission/comment_ids/" + submission_id)
        return resp.get("data")

    def get_comments_from_reddit_api(self, comment_ids):
        headers = {'User-agent': 'Comment Collector'}
        comments: List[RedditComment] = []
        for index in range(0, len(comment_ids), 100):
            params = dict()

            # TODO: Rate limiting here for reddit

            params['id'] = ','.join(["t1_" + i for i in comment_ids[index: min(index + 100, len(comment_ids))]])
            r = self._reddit_session.get("https://api.reddit.com/api/info", params=params, headers=headers)
            data = r.json()
            for c in data['data']['children']:
                comments.append(
                    RedditComment(
                        c['data']['id'],
                        c['data']['author_fullname'],
                        datetime.datetime.utcfromtimestamp(int(c['data']['created_utc']))
                    )
                )
        return comments

    def get_comments(self, ids: List) -> List[RedditComment]:
        if len(ids) == 0:
            return []
        comments: List[RedditComment] = []
        for index in range(0, len(ids), 500):
            params = {
                'ids': ids[index: min(index + 500, len(ids))],
                'sort': 'desc',
                'size': 500,
                'sort_type': 'created_utc',
            }
            resp = self._api_call("/search/comment", params)
            j = json.loads(resp.text)
            comments.append(j)
        return comments

    def get_num_comments(self, date: datetime.date) -> int:
        dt = datetime.datetime(date.year, date.month, date.day, hour=17)
        after = dt.timestamp()
        before = (dt + datetime.timedelta(days=1)).timestamp()
        params = {
            'subreddit': self._subreddit,
            'aggs': 'subreddit',
            'before': int(before),
            'after': int(after),
            'size': 0
        }
        resp = self._api_call('/search/comment', params)
        return resp['aggs']['subreddit'][0]['doc_count']

    def get_submissions(self, before: int = None, after: int = None) -> Union[None, List[RedditPost]]:
        params = {
            'subreddit': self._subreddit,
            'sort': 'desc',
            'size': 500,
            'sort_type': 'created_utc',
        }
        # before and after flipped here since we are interested in posts after, and posts before a certain date
        if before is not None and after is not None and before <= after:
            return []
        if before is not None:
            params.update({'before': int(before - 1)})
        if after is not None:
            params.update({'after': int(after)})
        resp = self._api_call("/search/submission", params)
        if resp is None:
            return resp
        submissions = resp["data"]
        return [RedditPost(s['id'], s['author'], s['created_utc'], s['num_comments']) for s in submissions]

    def _get_rate_limit(self) -> int:
        resp = self._api_call("/meta")
        j = json.loads(resp.text)
        return j.get("server_ratelimit_per_minute") - 2
